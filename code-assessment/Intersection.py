def intersection(list_1, list_2):
    return list(set(list_1) & set(list_2))


list1 = [1, 2, 3, 5, 7, 27, 35, 4, 81]
list2 = [2, 33, 2, 34, 27, 23, 24, 81, 1, 7, 27]
list3 = intersection(list1, list2)

print(list3)
