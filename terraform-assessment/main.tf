module "networking" {
  source    = "modules/network"
  namespace = var.namespace
}

module "ec2" {
  source                    = "./modules/ec2"
  namespace                 = var.namespace
  vpc                       = module.networking.vpc
  security_group_public_id  = module.networking.security_group_public_id
}