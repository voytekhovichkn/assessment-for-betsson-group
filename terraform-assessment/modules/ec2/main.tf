data "aws_ami" "amazon-linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

// Create EC2 in a public subnet
resource "aws_instance" "ec2_public_subnet" {
  ami                         = data.aws_ami.amazon-linux.id
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  subnet_id                   = var.vpc.public_subnets[0]
}