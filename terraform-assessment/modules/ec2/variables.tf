variable "namespace" {
  type = string
}

variable "vpc" {
  type = any
}

variable "security_group_public_id" {
  type = any
}