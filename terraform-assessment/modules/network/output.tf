output "vpc" {
  value = module.vpc
}

output "security_group_public_id" {
  value = aws_security_group.allow_ssh.id
}