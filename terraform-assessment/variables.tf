variable "namespace" {
  description = "Namespace"
  default     = "terraform-assessment-namespace"
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string
}